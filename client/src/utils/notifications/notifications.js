import { createToast } from "mosha-vue-toastify";
export const successNotify = (message, delay) => {
  createToast("Success! " + message, {
    type: "success",
    position: "top-center",
    showIcon: true,
    hideProgressBar: true,
    transition: "zoom",
    timeout: delay ? delay * 1000 : 5000,
  });
};
export const unsuccessNotify = (message, delay) => {
  createToast("False! " + message, {
    type: "warning",
    position: "top-center",
    showIcon: true,
    hideProgressBar: true,
    transition: "zoom",
    timeout: delay ? delay * 1000 : 5000,
  });
};
export const infoNotify = (message, delay) => {
  createToast(message, {
    type: "info",
    position: "top-center",
    showIcon: true,
    hideProgressBar: true,
    transition: "slide",
    timeout: delay ? delay * 1000 : 5000,
  });
};
