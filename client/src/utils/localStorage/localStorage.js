export const addLocalStorage = (key, value) => {
  localStorage.setItem(key, value);
};
export const getLocalStorage = (key) => {
  localStorage.getItem(key);
};
export const delLocalStorage = (key) => {
  localStorage.removeItem(key);
};
