import { createApp } from "vue";
import store from "./store/index";
import router from "./routes";
import App from "./App.vue";
import "./notifystyle.css";
import "./style.css";
import "./utils/validations/validationRules";
import "./utils/localStorage/localStorage";
createApp(App)
  .use(router)
  .use(store)
  .mount("#app");
