import { createStore } from "vuex";
import {
  addLocalStorage,
  getLocalStorage,
  delLocalStorage,
} from "../utils/localStorage/localStorage";
import format from "date-fns/format";

const store = createStore({
  state: {
    user: [],
    links: [],
    link: [],
    todo: [],
  },
  getters: {
    getUser(state) {
      return state.user;
    },
    getAllLinks(state) {
      return state.links;
    },
    getLink(state) {
      return state.link;
    },
    getTodo(state) {
      return state.todo;
    },
    getTodoByCompleted(state) {
      return state.todo.filter((v) => v.completed == true);
    },
    getTodoByImportant(state) {
      return state.todo.filter((v) => v.important == true);
    },
    getTodoByNotStatus(state) {
      return state.todo.filter((v) => !v.completed && !v.important);
    },
  },
  mutations: {
    setUser(state, payload) {
      state.user = payload[1];
      delLocalStorage(payload[0]);
      addLocalStorage(payload[0], JSON.stringify(state.user));
    },
    delUser(state, userKey) {
      state.user.length = 0;
      delLocalStorage(userKey);
    },
    setLink(state, link) {
      state.links.push(link);
    },
    setAllLinks(state, links) {
      state.links = links;
      state.links.map((v) => (v.date = format(Date.parse(v.date), "PPpp")));
    },
    setLinkById(state, link) {
      state.link = link;
    },
    setTodos(state, todos) {
      state.todo = todos;
      state.todo.map((v) => (v.date = format(Date.parse(v.date), "PPpp")));
    },
  },
  actions: {
    setUser({ commit }, payload) {
      commit("setUser", payload);
    },
    delUser({ commit }, userKey) {
      commit("delUser", userKey);
    },
    setLink({ commit }, link) {
      commit("setLink", link);
    },
    setAllLinks({ commit }, links) {
      commit("setAllLinks", links);
    },
    setLinkById({ commit }, link) {
      commit("setLinkById", link);
    },
    setTodos({ commit }, todos) {
      commit("setTodos", todos);
    },
  },
});
export default store;
