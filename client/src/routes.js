import { createRouter, createWebHistory } from "vue-router";
const routerHistory = createWebHistory();
import HomePage from "@/pages/HomePage";
import LoginPage from "@/pages/LoginPage";
import RegistrationPage from "@/pages/RegistrationPage";
import User from "@/pages/UserPage";
import LinkPage from "@/pages/LinkPage";
const router = createRouter({
  history: routerHistory,
  routes: [
    {
      path: "/",
      name: "home",
      component: HomePage,
    },
    {
      path: "/registration",
      name: "registration",
      component: RegistrationPage,
    },
    {
      path: "/login",
      name: "login",
      component: LoginPage,
    },
    {
      path: "/user",
      name: "user",
      component: User,
    },
    {
      path: "/user/link/:id",
      name: "userLink",
      component: LinkPage,
    },
  ],
});
export default router;
