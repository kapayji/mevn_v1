import Link from "../../models/link/index.js";
import shortid from "shortid";
import config from "config";

export async function generateLink(req, res) {
  try {
    const { from } = req.body;
    const code = shortid.generate();
    const baseUrl = config.get("baseUrl");
    const to = baseUrl + "/t/" + code;
    const existing = await Link.findOne({ from, owner: req.user.userId });
    if (existing) {
      return res.json({ link: existing });
    }
    const link = new Link({ from, to, code, owner: req.user.userId });
    await link.save();
    res.status(201).json({ link });
  } catch (e) {
    res.status(500).json({ message: "Something went wrong, try again" });
  }
}
export async function getLinks(req, res) {
  try {
    const links = await Link.find({ owner: req.user.userId });
    res.json(links);
  } catch (e) {
    res.status(500).json({ message: "Something went wrong, try again" });
  }
}
export async function getLink(req, res) {
  try {
    const link = await Link.findById(req.params.id);
    res.json(link);
  } catch (e) {
    res.status(500).json({ message: "Something went wrong, try again" });
  }
}
export async function deleteLink(req, res) {
  try {
    await Link.findOneAndDelete({ _id: req.params.id });
    res.json({ message: "Link delete successefully" });
  } catch (e) {
    console.log(e);
  }
}
