import Todo from "../../models/todo/index.js";

export async function getTodos(req, res) {
  try {
    const todos = await Todo.find({ owner: req.user.userId });
    if (!todos) {
      res.status(400).json({ message: "Nothing found" });
    }
    res.status(201).json(todos);
  } catch (e) {
    res.status(500).json({ message: `Something wrong - ${e}` });
  }
}
export async function createToDo(req, res) {
  try {
    const { title } = req.body;
    const todo = new Todo({ title, owner: req.user.userId });
    await todo.save();
    res.status(201).json({ message: "Todo creation successeful" });
  } catch (e) {
    res.status(500).json({ message: `Create ToDo error ${e}` });
  }
}

export async function updateToDoCompleted(req, res) {
  try {
    const { todoId } = req.body;
    console.log(todoId);
    const todo = await Todo.findOne({ _id: todoId, owner: req.user.userId });
    todo.completed = !todo.completed;
    await todo.save();
    res.json(todo);
  } catch (e) {
    res.status(500).json({ message: `Update ToDo error ${e}` });
  }
}
export async function updateToDoImportanted(req, res) {
  try {
    const { todoId } = req.body;
    const todo = await Todo.findOne({ _id: todoId, owner: req.user.userId });
    todo.important = !todo.important;
    await todo.save();
    res.json(todo);
  } catch (e) {
    res.status(500).json({ message: `Update ToDo error ${e}` });
  }
}
export async function updateToDoRemove(req, res) {
  try {
    const { todoId } = req.body;
    const todo = await Todo.findOneAndDelete({
      _id: todoId,
      owner: req.user.userId,
    });
    res.json(todo);
  } catch (e) {
    res.status(500).json({ message: `Update ToDo error ${e}` });
  }
}
