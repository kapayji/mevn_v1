import Link from "../../models/link/index.js";

export async function redirectLink(req, res) {
  try {
    const link = await Link.findOne({ code: req.params.code });
    if (link) {
      link.clicks++;
      await link.save();
      // res.setHeader("Access-Control-Allow-Origin", "http://localhost:5000/");
      console.log(req.params.code);
      // return res.redirect(301, link.from);
    }
    res.status(404).json({ message: "Link not found" });
  } catch (e) {
    res.status(500).json({ message: "Something went wrong, try again" });
  }
}
