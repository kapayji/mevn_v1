import mongoose from "mongoose";

const Link = new mongoose.Schema({
  from: { type: String, require: true },
  to: { type: String, require: true, unique: true },
  code: { type: String, require: true, unique: true },
  clicks: { type: Number, default: 0 },
  date: { type: Date, default: Date.now },
  owner: { type: mongoose.ObjectId, ref: "User" },
});
export default mongoose.model("Link", Link);
