import mongoose from "mongoose";

const Todo = new mongoose.Schema({
  date: { type: Date, default: Date.now },
  title: { type: String, require: true },
  owner: { type: mongoose.ObjectId, ref: "User" },
  completed: { type: Boolean, default: false },
  important: { type: Boolean, default: false },
});

export default mongoose.model("Todo", Todo);
