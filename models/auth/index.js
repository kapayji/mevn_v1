import mongoose from "mongoose";

const User = new mongoose.Schema({
  email: { type: String, require: true, unique: true },
  password: { type: String, require: true },
  nickname: { type: String, require: true },
  sex: { type: String, require: true },
  link: [{ type: mongoose.ObjectId, ref: "Link" }],
});

export default mongoose.model("User", User);
