import express from "express";
// import bodyParser from "body-parser";
import config from "config";
import mongoose from "mongoose";
import cors from "cors";

import { router } from "./routes/auth/auth.routes.js";
import { routerLink } from "./routes/link/link.routes.js";
import { redirectTo } from "./routes/redirect/redirect.routes.js";
import { routerTodo } from "./routes/todo/todo.routes.js";

const app = express();
const PORT = config.get("port") || 5000;
const MONGO_DB_URL = config.get("mongoDbUrl");

app.use(
  cors({
    origin: "*",
  })
);

// app.use(cors());

app.use(
  express.json({
    extended: true,
  })
);

// app.use(bodyParser.json());
app.use("/api/auth/", router);
app.use("/api/link", routerLink);
app.use("/t", redirectTo);
app.use("/api/todo", routerTodo);

async function start() {
  try {
    await mongoose.connect(MONGO_DB_URL, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false,
    });
    app.listen(PORT, () => {
      console.log(`App has been started on port ${PORT}`);
    });
  } catch (e) {
    console.log("error Server", e.message);
    process.exit(1);
  }
}
start();
