import Router from "express";
import {
  getTodos,
  createToDo,
  updateToDoCompleted,
  updateToDoImportanted,
  updateToDoRemove,
} from "../../controllers/todo/todo.js";
import { authMiddleware } from "../../middleware/auth.middleware.js";

export const routerTodo = Router();

routerTodo.get("/", authMiddleware, async (req, res) => {
  getTodos(req, res);
});
routerTodo.post("/create", authMiddleware, async (req, res) => {
  createToDo(req, res);
});
routerTodo.put("/completed", authMiddleware, async (req, res) =>
  updateToDoCompleted(req, res)
);
routerTodo.put("/importanted", authMiddleware, async (req, res) =>
  updateToDoImportanted(req, res)
);
routerTodo.put("/remove", authMiddleware, async (req, res) =>
  updateToDoRemove(req, res)
);
