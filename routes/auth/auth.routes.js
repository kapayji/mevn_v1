import Router from "express";
import validator from "express-validator";
const { check } = validator;
import { registration } from "../../controllers/auth/registration.js";
import { login } from "../../controllers/auth/login.js";

export const router = Router();
router.post(
  "/registration",
  [
    check("email", "Incorrect email").isEmail(),
    check("password", "Minimal 6 characters").isLength({ min: 6 }),
  ],
  async (req, res) => {
    registration(req, res);
  }
);
router.post(
  "/login",
  [
    check("email", "Input correct email").normalizeEmail().isEmail(),
    check("password", "Введите пароль").exists(),
  ],
  async (req, res) => login(req, res)
);
