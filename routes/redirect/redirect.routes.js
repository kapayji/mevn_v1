import Router from "express";
import { redirectLink } from "../../controllers/redirect/redirect.js";
import { authMiddleware } from "../../middleware/auth.middleware.js";

export const redirectTo = Router();

redirectTo.get("/:code", async (req, res) => {
  redirectLink(req, res);
});
