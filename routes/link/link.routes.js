import Router from "express";
import {
  generateLink,
  getLink,
  getLinks,
  deleteLink,
} from "../../controllers/link/index.js";
import { authMiddleware } from "../../middleware/auth.middleware.js";

export const routerLink = Router();

routerLink.post("/generate", authMiddleware, async (req, res) => {
  generateLink(req, res);
});
routerLink.get("/", authMiddleware, async (req, res) => {
  getLinks(req, res);
});
routerLink.get("/:id", authMiddleware, async (req, res) => {
  getLink(req, res);
});
routerLink.delete("/:id", authMiddleware, async (req, res) => {
  deleteLink(req, res);
});
